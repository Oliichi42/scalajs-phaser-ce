package com.gitlab.oliichi42.phaserce.binding.component

import com.gitlab.oliichi42.phaserce.binding.input.InputHandler

import scala.scalajs.js

@js.native
trait InputEnabled extends js.Object {
  var input: InputHandler = js.native

  var inputEnabled: Boolean = js.native
}
