package com.gitlab.oliichi42.phaserce.binding.gameobjects

import com.gitlab.oliichi42.phaserce.binding.pixi.DisplayObject
import com.gitlab.oliichi42.phaserce.binding.core.{Filter, Group, Plugin}
import com.gitlab.oliichi42.phaserce.binding.geom.Point
import com.gitlab.oliichi42.phaserce.binding.particles.arcade.Emitter
import com.gitlab.oliichi42.phaserce.binding.plugins.weapons.Weapon
import com.gitlab.oliichi42.phaserce.binding.sound.{AudioSprite, Sound}
import com.gitlab.oliichi42.phaserce.binding.tilemap.Tilemap
import com.gitlab.oliichi42.phaserce.binding.tween.Tween

import scala.scalajs.js
import scala.scalajs.js.annotation.JSGlobal
import scala.scalajs.js.{UndefOr, undefined, |}

@js.native
@JSGlobal("Phaser.GameObjectFactory")
class GameObjectFactory extends js.Object {

  def existing(obj: DisplayObject): js.Any = js.native

  def weapon(quantity: UndefOr[Int] = undefined, key: UndefOr[String | js.Object] = undefined, frame: UndefOr[String | Int] = undefined, group: UndefOr[Group] = undefined, bulletClass: UndefOr[js.Function] = undefined): Weapon = js.native

  def image(x: UndefOr[Double] = undefined, y: UndefOr[Double] = undefined, key: UndefOr[String | js.Object] = undefined, frame: UndefOr[String | Int] = undefined, group: UndefOr[Group] = undefined): Image = js.native

  def sprite(x: UndefOr[Double] = undefined, y: UndefOr[Double] = undefined, key: UndefOr[String | js.Object] = undefined, frame: UndefOr[String | Int] = undefined, group: UndefOr[Group] = undefined): Sprite = js.native

  // def creature()

  def tween(obj: js.Object): Tween = js.native

  def group(parent: js.Any = undefined, name: UndefOr[String] = undefined, addToStage: UndefOr[Boolean] = undefined, enableBody: UndefOr[Boolean] = undefined, physicsBodyType: UndefOr[Int] = undefined): Group = js.native

  def physicsGroup(physicsBodyType: UndefOr[Int] = undefined, parent: UndefOr[js.Any] = undefined, name: UndefOr[String] = undefined, addToStage: UndefOr[Boolean] = undefined): Group = js.native

  def spriteBatch(parent: UndefOr[Group] = undefined, name: UndefOr[String] = undefined, addToStage: UndefOr[Boolean] = undefined): SpriteBatch = js.native

  def audio(key: String, volume: UndefOr[Double] = undefined, loop: UndefOr[Boolean] = undefined, connect: UndefOr[Boolean] = undefined): Sound = js.native

  def sound(key: String, volume: UndefOr[Double] = undefined, loop: UndefOr[Boolean] = undefined, connect: UndefOr[Boolean] = undefined): Sound = js.native

  def audioSprite(key: String): AudioSprite = js.native

  def tileSprite(x: Double, y: Double, width: Double, height: Double, key: String | js.Object, frame: UndefOr[String | Int] = undefined, group: UndefOr[Group] = undefined): TileSprite = js.native

  def rope(x: UndefOr[Double] = undefined, y: UndefOr[Double] = undefined, key: UndefOr[String | js.Object] = undefined, frame: UndefOr[String | Int] = undefined, points: UndefOr[js.Array[Point]] = undefined, group: UndefOr[Group] = undefined): Rope = js.native

  def text(x: UndefOr[Double] = undefined, y: UndefOr[Double] = undefined, text: UndefOr[String] = undefined, style: UndefOr[js.Object] = undefined, group: UndefOr[Group] = undefined): Text = js.native

  def button(x: UndefOr[Double] = undefined, y: UndefOr[Double] = undefined, key: UndefOr[String] = undefined, callback: UndefOr[js.Function] = undefined, callbackContext: UndefOr[js.Object] = undefined, overFrame: UndefOr[String | Int] = undefined, outFrame: UndefOr[String | Int] = undefined, downFrame: UndefOr[String | Int] = undefined, upFrame: UndefOr[String | Int] = undefined): Button = js.native

  def graphics(x: UndefOr[Double] = undefined, y: UndefOr[Double] = undefined, group: UndefOr[Group] = undefined): Graphics = js.native
  
  def emitter(x: UndefOr[Double] = undefined, y: UndefOr[Double] = undefined, maxParticles: UndefOr[Int] = undefined): Emitter = js.native

  def retroFont(font: String, characterWidth: Int, characterHeight: Int, chars: String, charsPerRow: Int, xSpacing: UndefOr[Double] = undefined, ySpacing: UndefOr[Double] = undefined, xOffset: UndefOr[Double] = undefined, yOffset: UndefOr[Double] = undefined): RetroFont = js.native

  def bitmapText(x: Double, y: Double, font: String, text: UndefOr[String] = undefined, size: UndefOr[Int] = undefined, group: UndefOr[Group] = undefined): BitmapText = js.native

  def tilemap(key: UndefOr[String] = undefined, tileWidth: UndefOr[Int] = undefined, tileHeight: UndefOr[Int] = undefined, width: UndefOr[Int] = undefined, height: UndefOr[Int] = undefined): Tilemap = js.native

  def renderTexture(width: UndefOr[Int] = undefined, height: UndefOr[Int] = undefined, key: UndefOr[String] = undefined, addToCache: UndefOr[Boolean] = undefined): RenderTexture = js.native

  def video(key: UndefOr[String] = undefined, url: UndefOr[String] = undefined): Video = js.native

  def bitmapData(width: UndefOr[Int] = undefined, height: UndefOr[Int] = undefined, key: UndefOr[String] = undefined, addToCache: UndefOr[Boolean] = undefined): BitmapData = js.native

  def filter(filter: String, params: js.Any*): Filter = js.native

  def plugin(plugin: Plugin | js.Object, initParams: js.Any*): Plugin = js.native

}
