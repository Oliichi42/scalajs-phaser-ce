package com.gitlab.oliichi42.phaserce.binding.loader

import com.gitlab.oliichi42.phaserce.binding.core.{Game, Signal}
import com.gitlab.oliichi42.phaserce.binding.gameobjects.{
  BitmapData,
  Image,
  Sprite
}

import scala.scalajs.js
import scala.scalajs.js.annotation.JSGlobal
import scala.scalajs.js.{UndefOr, undefined, |}

@js.native
@JSGlobal("Phaser.Loader")
class Loader(game: Game) extends js.Object {

  var resetLocked: Boolean = js.native
  var isLoading: Boolean = js.native
  var hasLoaded: Boolean = js.native

  /**
    * Boolean or String
    */
  var crossOrigin: Boolean | String = js.native
  var baseURL: String = js.native
  var path: String = js.native
  var headers: js.Dictionary[Boolean | String] = js.native
  var onLoadStart: Signal[_] = js.native
  var onLoadComplete: Signal[_] = js.native
  var onPackComplete: Signal[_] = js.native
  var onFileStart: Signal[_] = js.native
  var onFileComplete: Signal[_] = js.native
  var onFileError: Signal[_] = js.native
  var enableParallel: Boolean = js.native
  var maxParallelDownloads: Int = js.native

  def setPreloadSprite(sprite: Sprite, direction: Int): Unit = js.native
  def setPreloadSprite(sprite: Image, direction: Int): Unit = js.native

  def checkKeyExists(kind: String, key: String): Boolean = js.native

  def getAssetIndex(kind: String, key: String): Int = js.native

  // TODO wrap with Option
  def getAsset(kind: String, key: String): js.Any = js.native

  def pack(key: String,
           url: String,
           data: js.Object,
           callbackContext: js.Object): Loader = js.native

  def image(key: String): Loader = js.native
  def image(key: String, url: String | js.Object): Loader = js.native
  def image(key: String, url: String | js.Object, overwrite: Boolean): Loader =
    js.native

  def imageFromBitmapData(key: String, bitmapData: BitmapData): Loader =
    js.native
  def imageFromBitmapData(key: String,
                          bitmapData: BitmapData,
                          overwrite: Boolean): Loader = js.native

  def texture(key: String, obj: js.Object): Loader = js.native
  def texture(key: String, obj: js.Object, overwrite: Boolean): Loader =
    js.native

  def images(keys: js.Array[String]): Loader = js.native
  def images(keys: js.Array[String], urls: js.Array[String]): Loader = js.native

  def text(key: String): Loader = js.native
  def text(key: String, url: String): Loader = js.native
  def text(key: String, url: String, overwrite: Boolean): Loader = js.native

  def json(key: String): Loader = js.native
  def json(key: String, url: String): Loader = js.native
  def json(key: String, url: String, overwrite: Boolean): Loader = js.native

  def shader(key: String): Loader = js.native
  def shader(key: String, url: String): Loader = js.native
  def shader(key: String, url: String, overwrite: Boolean): Loader = js.native

  def xml(key: String): Loader = js.native
  def xml(key: String, url: String): Loader = js.native
  def xml(key: String, url: String, overwrite: Boolean): Loader = js.native

  def script(key: String): Loader = js.native
  def script(key: String, url: String): Loader = js.native
  def script(key: String, url: String, overwrite: Boolean): Loader = js.native
  def script(key: String,
             url: String,
             overwrite: Boolean,
             callbackContext: js.Object): Loader = js.native

  def binary(key: String): Loader = js.native
  def binary(key: String, url: String): Loader = js.native
  def binary(key: String, url: String, overwrite: Boolean): Loader = js.native
  def binary(key: String,
             url: String,
             overwrite: Boolean,
             callbackContext: js.Object): Loader = js.native

  def spritesheet(key: String,
                  url: String,
                  frameWidth: Int,
                  frameHeight: Int,
                  frameMax: Int,
                  margin: Int,
                  spacing: Int,
                  skipFrames: Int): Loader = js.native
  def spritesheet(key: String,
                  url: String,
                  frameWidth: Int,
                  frameHeight: Int,
                  frameMax: Int,
                  margin: Int,
                  spacing: Int): Loader = js.native
  def spritesheet(key: String,
                  url: String,
                  frameWidth: Int,
                  frameHeight: Int,
                  frameMax: Int,
                  margin: Int): Loader = js.native
  def spritesheet(key: String,
                  url: String,
                  frameWidth: Int,
                  frameHeight: Int,
                  frameMax: Int): Loader = js.native
  def spritesheet(key: String,
                  url: String,
                  frameWidth: Int,
                  frameHeight: Int): Loader = js.native

  def audio(key: String,
            urls: String | js.Array[String] | js.Any,
            autoDecode: UndefOr[Boolean] = undefined): Loader = js.native
  def audiosprite(key: String,
                  urls: js.Array[String],
                  jsonUrl: UndefOr[String] = undefined,
                  jsonData: UndefOr[String | js.Any] = undefined,
                  autoDecode: UndefOr[Boolean] = undefined): Loader = js.native

}

@js.native
@JSGlobal("Phaser.Loader")
object Loader extends js.Function {
  val TEXTURE_ATLAS_JSON_ARRAY: Int = js.native
  val TEXTURE_ATLAS_JSON_HASH: Int = js.native
  val TEXTURE_ATLAS_XML_STARLING: Int = js.native
  val PHYSICS_LIME_CORONA_JSON: Int = js.native
  val PHYSICS_PHASER_JSON: Int = js.native
  val TEXTURE_ATLAS_JSON_PIXEL: Int = js.native
}
