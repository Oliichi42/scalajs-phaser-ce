package com.gitlab.oliichi42.phaserce.binding.sound

import com.gitlab.oliichi42.phaserce.binding.core.{Game, Signal}

import scala.scalajs.js
import scala.scalajs.js.{UndefOr, undefined}
import scala.scalajs.js.annotation.JSGlobal

@JSGlobal("Phaser.SoundManager")
@js.native
class SoundManager extends js.Object {
  var channels: Int = js.native
  var connectToMaster: Boolean = js.native
  var context: js.Any = js.native
  var game: Game = js.native
  var mute: Boolean = js.native
  var muteOnPause: Boolean = js.native
  var noAudio: Boolean = js.native
  var onSoundDecode: Signal[_] = js.native
  var onVolumeChange: Signal[_] = js.native
  var onMute: Signal[_] = js.native
  var onUnMute: Signal[_] = js.native
  var touchLocked: Boolean = js.native
  var usingAudioTag: Boolean = js.native
  var usingWebAudio: Boolean = js.native
  var volume: Double = js.native
  def add(key: String, volume: UndefOr[Double] = undefined, loop: UndefOr[Boolean] = undefined, connect: UndefOr[Boolean] = undefined): Sound = js.native
  def addSprite(key: String): AudioSprite = js.native
  def boot(): Unit = js.native
  def decode(key: String, sound: UndefOr[Sound] = undefined): Unit = js.native
  def destroy(): Unit = js.native
  def pauseAll(): Unit = js.native
  def play(key: String, volume: UndefOr[Double] = undefined, loop: UndefOr[Boolean] = undefined): Sound = js.native
  def remove(sound: Sound): Boolean = js.native
  def removeByKey(key: String): Int = js.native
  def resumeAll(): Unit = js.native
  def setTouchLock(): Unit = js.native
  def stopAll(): Unit = js.native
  def unlock(): Boolean = js.native
  def update(): Unit = js.native
}
