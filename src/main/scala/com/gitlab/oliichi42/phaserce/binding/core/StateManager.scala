package com.gitlab.oliichi42.phaserce.binding.core

import scala.scalajs.js
import scala.scalajs.js.{UndefOr, undefined, |}

@js.native
trait StateManager extends js.Object {
  var game: Game = js.native
  var states: js.Dictionary[State] = js.native
  var current: String = js.native

  var onStateChange: Signal[_] = js.native

  var onInitCallback: js.Function = js.native
  var onPreloadCallback: js.Function = js.native
  var onCreateCallback: js.Function = js.native
  var onUpdateCallback: js.Function = js.native
  var onRenderCallback: js.Function = js.native
  var onResizeCallback: js.Function = js.native
  var onPreRenderCallback: js.Function = js.native
  var onLoadUpdateCallback: js.Function = js.native
  var onLoadRenderCallback: js.Function = js.native
  var onPausedCallback: js.Function = js.native
  var onResumedCallback: js.Function = js.native
  var onPauseUpdateCallback: js.Function = js.native
  var onShutdownCallback: js.Function = js.native

  def add(key: String, state: State | js.Object | js.Function, autoStart: UndefOr[Boolean] = undefined): Unit = js.native

  def remove(key: String): Unit = js.native

  def start(key: String, clearWorld: UndefOr[Boolean] = undefined, clearCache: UndefOr[Boolean] = undefined): Unit = js.native
  def start(key: String, clearWorld: UndefOr[Boolean], clearCache: UndefOr[Boolean], initParams: js.Any*): Unit = js.native

  def restart(clearWorld: UndefOr[Boolean] = undefined, clearCache: UndefOr[Boolean] = undefined): Unit = js.native
  def restart(clearWorld: UndefOr[Boolean], clearCache: UndefOr[Boolean], initParams: js.Any*): Unit = js.native

  // preUpdate doesn't look like it should be public, clearCurrentState

  def checkState(key: String): Boolean = js.native

  def getCurrentState: State = js.native

  def destroy(): Unit = js.native

  def created: Boolean = js.native



}
