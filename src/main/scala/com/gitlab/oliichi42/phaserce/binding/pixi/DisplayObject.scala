package com.gitlab.oliichi42.phaserce.binding.pixi

import com.gitlab.oliichi42.phaserce.binding.core.Filter
import com.gitlab.oliichi42.phaserce.binding.gameobjects.Graphics
import com.gitlab.oliichi42.phaserce.binding.geom._

import scala.scalajs.js
import scala.scalajs.js.|

@js.native
trait DisplayObject extends js.Object {

  var position: Point = js.native
  var scale: Point = js.native
  var pivot: Point = js.native

  /*
  Rotation in degrees.
   */
  var rotation: Double = js.native

  var alpha: Double = js.native
  var visible: Boolean = js.native
  var hitArea: Rectangle | Circle | Ellipse | Polygon = js.native

  var renderable: Boolean = js.native

  // def parent: DisplayObjectContainer

  def worldAlpha: Double = js.native

  def worldTransform: Matrix = js.native

  def worldPosition: Point = js.native

  def worldScale: Point = js.native

  def worldRotation: Double = js.native

  var filterArea: Rectangle = js.native

  def destroy(): Unit = js.native

  // def updateTransform(parent)
  // preUpdate
  // generateTexture
  // updateCache
  // toGlobal
  // toLocal

  var x, y: Double = js.native
  def worldVisible: Boolean = js.native
  var mask: Graphics = js.native
  var filters: js.Array[Filter] = js.native
  var cacheAsBitmap: Boolean = js.native

}
