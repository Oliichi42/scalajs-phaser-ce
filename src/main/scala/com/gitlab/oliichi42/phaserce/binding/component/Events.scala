package com.gitlab.oliichi42.phaserce.binding.component

import com.gitlab.oliichi42.phaserce.binding.component.Events.{NullarySignal, PointerSignal}
import com.gitlab.oliichi42.phaserce.binding.core.Signal
import com.gitlab.oliichi42.phaserce.binding.gameobjects.Sprite
import com.gitlab.oliichi42.phaserce.binding.input.Pointer

import scala.scalajs.js

@js.native
trait Events extends js.Object {
  def parent: Sprite = js.native

  def destroy(): Unit = js.native


  var onAddedToGroup: Signal[_] = js.native
  var onRemovedFromGroup: Signal[_] = js.native
  var onRemovedFromWorld: Signal[_] = js.native
  var onKilled: Signal[_] = js.native
  var onRevived: Signal[_] = js.native
  var onOutOfBounds: Signal[_] = js.native
  var onEnterBounds: NullarySignal = js.native
  var onInputOver: Signal[_] = js.native
  var onInputOut: Signal[_] = js.native
  var onInputDown: PointerSignal = js.native
  var onInputUp: PointerSignal = js.native
  var onDestroy: NullarySignal = js.native
  var onDragStart: Signal[_] = js.native
  var onDragStop: Signal[_] = js.native
  var onDragUpdate: Signal[_] = js.native
  var onAnimationStart: Signal[_] = js.native
  var onAnimationComplete: Signal[_]= js.native
  var onAnimationLoop: Signal[_] = js.native
}

object Events {
  type NullarySignal = Signal[js.Function1[js.Any, Unit]]

  type PointerSignal = Signal[js.Function2[js.Any, Pointer, Unit]]
}