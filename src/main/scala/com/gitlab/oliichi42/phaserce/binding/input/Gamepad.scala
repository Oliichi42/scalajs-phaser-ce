package com.gitlab.oliichi42.phaserce.binding.input

import com.gitlab.oliichi42.phaserce.binding.core.Game

import scala.scalajs.js
import scala.scalajs.js.annotation.JSGlobal
import scala.scalajs.js.{UndefOr, undefined}

@js.native
trait Gamepad extends js.Object {
  var active: Boolean = js.native
  var callbackContext: js.Any = js.native
  var enabled: Boolean = js.native
  var game: Game = js.native
  var onAxisCallBack: js.Function = js.native
  var onConnectCallback: js.Function = js.native
  var onDisconnectCallback: js.Function = js.native
  var onDownCallback: js.Function = js.native
  var onFloatCallback: js.Function = js.native
  var onUpCallback: js.Function = js.native
  var pad1: SinglePad = js.native
  var pad2: SinglePad = js.native
  var pad3: SinglePad = js.native
  var pad4: SinglePad = js.native
  var padsConnected: Int = js.native
  var supported: Boolean = js.native
  def addCallbacks(context: js.Any, callbacks: js.Any): Unit = js.native
  def isDown(buttonCode: Int): Boolean = js.native
  def justPressed(buttonCode: Int, duration: UndefOr[Double] = undefined): Boolean = js.native
  def justReleased(buttonCode: Int, duration: UndefOr[Double] = undefined): Boolean = js.native
  def reset(): Unit = js.native
  def setDeadZones(value: js.Any): Unit = js.native
  def start(): Unit = js.native
  def stop(): Unit = js.native
  def update(): Unit = js.native
}

@js.native
@JSGlobal("Phaser.Gamepad")
object Gamepad extends js.Object {
  val BUTTON_0: Int = js.native
  val BUTTON_1: Int = js.native
  val BUTTON_2: Int = js.native
  val BUTTON_3: Int = js.native
  val BUTTON_4: Int = js.native
  val BUTTON_5: Int = js.native
  val BUTTON_6: Int = js.native
  val BUTTON_7: Int = js.native
  val BUTTON_8: Int = js.native
  val BUTTON_9: Int = js.native
  val BUTTON_10: Int = js.native
  val BUTTON_11: Int = js.native
  val BUTTON_12: Int = js.native
  val BUTTON_13: Int = js.native
  val BUTTON_14: Int = js.native
  val BUTTON_15: Int = js.native
  val AXIS_0: Int = js.native
  val AXIS_1: Int = js.native
  val AXIS_2: Int = js.native
  val AXIS_3: Int = js.native
  val AXIS_4: Int = js.native
  val AXIS_5: Int = js.native
  val AXIS_6: Int = js.native
  val AXIS_7: Int = js.native
  val AXIS_8: Int = js.native
  val AXIS_9: Int = js.native
  val XBOX360_A: Int = js.native
  val XBOX360_B: Int = js.native
  val XBOX360_X: Int = js.native
  val XBOX360_Y: Int = js.native
  val XBOX360_LEFT_BUMPER: Int = js.native
  val XBOX360_RIGHT_BUMPER: Int = js.native
  val XBOX360_LEFT_TRIGGER: Int = js.native
  val XBOX360_RIGHT_TRIGGER: Int = js.native
  val XBOX360_BACK: Int = js.native
  val XBOX360_START: Int = js.native
  val XBOX360_STICK_LEFT_BUTTON: Int = js.native
  val XBOX360_STICK_RIGHT_BUTTON: Int = js.native
  val XBOX360_DPAD_LEFT: Int = js.native
  val XBOX360_DPAD_RIGHT: Int = js.native
  val XBOX360_DPAD_UP: Int = js.native
  val XBOX360_DPAD_DOWN: Int = js.native
  val XBOX360_STICK_LEFT_X: Int = js.native
  val XBOX360_STICK_LEFT_Y: Int = js.native
  val XBOX360_STICK_RIGHT_X: Int = js.native
  val XBOX360_STICK_RIGHT_Y: Int = js.native
  val PS3XC_X: Int = js.native
  val PS3XC_CIRCLE: Int = js.native
  val PS3XC_SQUARE: Int = js.native
  val PS3XC_TRIANGLE: Int = js.native
  val PS3XC_L1: Int = js.native
  val PS3XC_R1: Int = js.native
  val PS3XC_L2: Int = js.native
  val PS3XC_R2: Int = js.native
  val PS3XC_SELECT: Int = js.native
  val PS3XC_START: Int = js.native
  val PS3XC_STICK_LEFT_BUTTON: Int = js.native
  val PS3XC_STICK_RIGHT_BUTTON: Int = js.native
  val PS3XC_DPAD_UP: Int = js.native
  val PS3XC_DPAD_DOWN: Int = js.native
  val PS3XC_DPAD_LEFT: Int = js.native
  val PS3XC_DPAD_RIGHT: Int = js.native
  val PS3XC_STICK_LEFT_X: Int = js.native
  val PS3XC_STICK_LEFT_Y: Int = js.native
  val PS3XC_STICK_RIGHT_X: Int = js.native
  val PS3XC_STICK_RIGHT_Y: Int = js.native
}
