package com.gitlab.oliichi42.phaserce.binding.input

import com.gitlab.oliichi42.phaserce.binding.core.Game

import scala.scalajs.js
import scala.scalajs.js.{UndefOr, undefined, |}

@js.native
trait SinglePad extends js.Object {
  var callbackContext: js.Any = js.native
  var connected: Boolean = js.native
  var deadZone: Double = js.native
  var game: Game = js.native
  var index: Int = js.native
  var onAxisCallback: js.Function = js.native
  var onConnectCallback: js.Function = js.native
  var onDisconnectCallback: js.Function = js.native
  var onDownCallback: js.Function = js.native
  var onFloatCallback: js.Function = js.native
  var onUpCallback: js.Function = js.native
  def axis(axisCode: Int): Double = js.native
  def addCallbacks(context: js.Any, callbacks: js.Any): Unit = js.native
  def buttonValue(buttonCode: Int): Int | Null = js.native
  def connect(rawPad: js.Any): Unit = js.native
  def destroy(): Unit = js.native
  def disconnect(): Unit = js.native
  def getButton(buttonCode: Int): DeviceButton = js.native
  def isDown(buttonCode: Int): Boolean = js.native
  def isUp(buttonCode: Int): Boolean = js.native
  def justPressed(buttonCode: Int, duration: UndefOr[Double] = undefined): Boolean = js.native
  def justReleased(buttonCode: Int, duration: UndefOr[Double] = undefined): Boolean = js.native
  def pollStatus(): Unit = js.native
  def processAxisChange(axisState: js.Any): Unit = js.native
  def processButtonDown(buttonCode: Int, value: js.Any): Unit = js.native
  def processButtonFloat(buttonCode: Int, value: js.Any): Unit = js.native
  def processButtonUp(buttonCode: Int, value: js.Any): Unit = js.native
  def reset(): Unit = js.native
}
