package com.gitlab.oliichi42.phaserce.binding.core

import com.gitlab.oliichi42.phaserce.binding.gameobjects.{GameObjectCreator, GameObjectFactory}
import com.gitlab.oliichi42.phaserce.binding.input.Input
import com.gitlab.oliichi42.phaserce.binding.loader.{Cache, Loader}
import com.gitlab.oliichi42.phaserce.binding.maths.{Maths, RandomDataGenerator}
import com.gitlab.oliichi42.phaserce.binding.particles.Particles
import com.gitlab.oliichi42.phaserce.binding.physics.Physics
import com.gitlab.oliichi42.phaserce.binding.sound.{Sound, SoundManager}
import com.gitlab.oliichi42.phaserce.binding.time.Time
import com.gitlab.oliichi42.phaserce.binding.tween.TweenManager

import scala.scalajs.js
import scala.scalajs.js.annotation.{JSGlobal, JSName}

@js.native
@JSGlobal("Phaser.State")
class State() extends js.Object {
  var game: Game = js.native
  var key: String = js.native
  var add: GameObjectFactory = js.native
  var make: GameObjectCreator = js.native
  var camera: Camera = js.native
  var cache: Cache = js.native
  var input: Input = js.native
  var load: Loader = js.native
  @JSName("math")
  var maths: Maths = js.native
  var sound: SoundManager = js.native
  var scale: ScaleManager = js.native
  var state: StateManager = js.native
  var time: Time = js.native
  var tweens: TweenManager = js.native
  var world: World = js.native
  var particles: Particles = js.native
  var physics: Physics = js.native
  var rnd: RandomDataGenerator = js.native

  def init(): Unit = js.native
  def preload(): Unit = js.native
  def loadUpdate(): Unit = js.native
  def loadRender(): Unit = js.native
  def create(): Unit = js.native
  def preRender(): Unit = js.native
  def render(): Unit = js.native
  def resize(): Unit = js.native
  def paused(): Unit = js.native
  def resumed(): Unit = js.native
  def pauseUpdate(): Unit = js.native
  def shutdown(): Unit = js.native

}
