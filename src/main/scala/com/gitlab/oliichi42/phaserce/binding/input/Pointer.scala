package com.gitlab.oliichi42.phaserce.binding.input

import com.gitlab.oliichi42.phaserce.binding.core.Game
import com.gitlab.oliichi42.phaserce.binding.geom.{Circle, Point}

import scala.scalajs.js
import scala.scalajs.js.{UndefOr, undefined}
import scala.scalajs.js.annotation.{JSGlobal, JSName}

@js.native
trait Pointer extends js.Object {
  var active: Boolean = js.native
  var backButton: DeviceButton = js.native
  var button: js.Any = js.native
  var circle: Circle = js.native
  var clientX, clientY: Int = js.native
  var dirty: Boolean = js.native
  var duration: Double = js.native
  var eraserButton: DeviceButton = js.native
  var exists: Boolean = js.native
  var forceOut: Boolean = js.native
  var forwardButton: DeviceButton = js.native
  var game: Game = js.native
  var id: Int = js.native
  var identifier: Int = js.native
  var interactiveCandidates: js.Array[InputHandler] = js.native
  var isDown: Boolean = js.native
  var isMouse: Boolean = js.native
  var isUp: Boolean = js.native
  var leftButton: DeviceButton = js.native
  var middleButton: DeviceButton = js.native
  var movementX, movementY: Int = js.native
  var msSinceLastClick: Int = js.native
  var pageX, pageY: Int = js.native
  var pointerId: Int = js.native
  var pointerMode: Int = js.native
  var position: Point = js.native
  var positionDown: Point = js.native
  var positionUp: Point = js.native
  var previousTapTime: Double = js.native
  var rawMovementX, rawMovementY: Int = js.native
  var rightButton: DeviceButton = js.native
  var screenX, screenY: Int = js.native
  var target: js.Any = js.native
  var targetObject: js.Any = js.native
  var timeDown: Double = js.native
  var timeUp: Double = js.native
  var totalTouches: Int = js.native
  @JSName("type")
  val kind: Int = js.native
  var withinGame: Boolean = js.native
  var worldX, worldY: Double = js.native
  var x, y: Double = js.native

  def addClickTrampoline(name: String, callback: js.Function, callbackContext: js.Any, callbackArgs: js.Any*): Unit = js.native
  def justPressed(duration: UndefOr[Double] = undefined): Boolean = js.native
  def justReleased(duration: UndefOr[Double] = undefined): Boolean = js.native
  def leave(event: js.Any): Unit = js.native
  def move(event: js.Any, fromClick: UndefOr[Boolean] = undefined): Unit = js.native
  def reset(): Unit = js.native
  def resetButtons(): Unit = js.native
  def resetMovement(): Unit = js.native
  def start(event: js.Any): Unit = js.native
  def stop(event: js.Any): Unit = js.native
  def swapTarget(newTarget: InputHandler, silent: UndefOr[Boolean] = undefined): Unit = js.native
  def update(): Unit = js.native
  // DOM required def updateButtons(event: MouseEvent): Unit = js.native
}

@JSGlobal("Phaser.Pointer")
@js.native
object Pointer extends js.Object {
  val NO_BUTTON: Int = js.native
  val LEFT_BUTTON: Int = js.native
  val RIGHT_BUTTON: Int = js.native
  val MIDDLE_BUTTON: Int = js.native
  val BACK_BUTTON: Int = js.native
  val FORWARD_BUTTON: Int = js.native
  val ERASER_BUTTON: Int = js.native
}