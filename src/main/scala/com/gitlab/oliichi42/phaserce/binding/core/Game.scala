package com.gitlab.oliichi42.phaserce.binding.core

import com.gitlab.oliichi42.phaserce.binding.gameobjects.{GameObjectCreator, GameObjectFactory}
import com.gitlab.oliichi42.phaserce.binding.input.Input
import com.gitlab.oliichi42.phaserce.binding.loader.{Cache, Loader}
import com.gitlab.oliichi42.phaserce.binding.maths.{Maths, RandomDataGenerator}
import com.gitlab.oliichi42.phaserce.binding.net.Net
import com.gitlab.oliichi42.phaserce.binding.physics.Physics
import com.gitlab.oliichi42.phaserce.binding.sound.SoundManager
import com.gitlab.oliichi42.phaserce.binding.time.Time
import com.gitlab.oliichi42.phaserce.binding.tween.TweenManager
import com.gitlab.oliichi42.phaserce.binding.utils.{Debug, Device}
import com.gitlab.oliichi42.phaserce.binding.Phaser

import scala.scalajs.js
import scala.scalajs.js.annotation.{JSGlobal, JSName}
import scala.scalajs.js.|

@JSGlobal("Phaser.Game")
@js.native
class Game(initWidth: Int = 800,
           initHeight: Int = 600,
           renderer: Int = Phaser.AUTO,
           canvasId: String = "",
          )
    extends js.Object {

  val id: Int = js.native

  // todo narrow
  val config: js.Object = js.native

  // todo narrow
  val physicsConfig: js.Object = js.native

  /**
    * NB string | HTMLElement
    */
  val parent: String | js.Object = js.native

  def width: Int = js.native
  def height: Int = js.native

  val resolution: Int = js.native

  var transparent: Boolean = js.native

  val antialias: Boolean = js.native

  val multiTexture: Boolean = js.native

  val preserveDrawingBuffer: Boolean = js.native

  var clearBeforeRender: Boolean = js.native

  def renderType: Int = js.native

  var state: StateManager = js.native

  def isBooted: Boolean = js.native

  def isRunning: Boolean = js.native

  var add: GameObjectFactory = js.native

  var make: GameObjectCreator = js.native

  var cache: Cache = js.native

  var input: Input = js.native

  var load: Loader = js.native

  @JSName("math")
  var maths: Maths = js.native

  var net: Net = js.native

  var scale: ScaleManager = js.native

  var sound: SoundManager = js.native

  var stage: Stage = js.native

  var time: Time = js.native

  var tweens: TweenManager = js.native

  var world: World = js.native

  var physics: Physics = js.native

  var plugins: PluginManager = js.native

  var rnd: RandomDataGenerator = js.native

  var device: Device = js.native

  var camera: Camera = js.native

  /**
    * HTMLCanvasElement
    */
  var canvas: js.Object = js.native

  // var context

  var debug: Debug = js.native

  var create: Create = js.native

  var lockRender: Boolean = js.native

  def stepping: Boolean = js.native
  def pendingStep: Boolean = js.native
  def stepCount: Boolean = js.native

  var onPause: Signal[_] = js.native
  var onResume: Signal[_] = js.native
  var onBlur: Signal[_] = js.native
  var onFocus: Signal[_] = js.native
  var fpsProblemNotifier: Signal[_] = js.native
  var forceSingleUpdate: Boolean = js.native








  def enableStep(): Unit = js.native
  def disableStep(): Unit = js.native
  def step(): Unit = js.native
  def destroy(): Unit = js.native



}
