package com.gitlab.oliichi42.phaserce.binding.tween

import com.gitlab.oliichi42.phaserce.binding.core.{Game, Signal}

import scala.scalajs.js
import scala.scalajs.js.{UndefOr, undefined, |}
import scala.scalajs.js.annotation.JSGlobal

@js.native
@JSGlobal("Phaser.Tween")
class Tween(targetArg: js.Object, gameArg: Game, managerArg: TweenManager)
    extends js.Object {
  var game: Game = js.native
  var target: js.Object = js.native
  var manager: TweenManager = js.native
  var timeline: js.Array[TweenData] = js.native
  var reverse: Boolean = js.native
  var timeScale: Double = js.native
  var repeatCounter: Int = js.native
  def pendingDelete: Boolean = js.native
  var onStart: Signal[_] = js.native
  var onLoop: Signal[_] = js.native
  var onRepeat: Signal[_] = js.native
  var onChildComplete: Signal[_] = js.native
  var onComplete: Signal[_] = js.native
  var isRunning: Boolean = js.native
  def current: Int = js.native
  var properties: js.Object = js.native
  var chainedTween: Tween = js.native
  var isPaused: Boolean = js.native
  var framebased: Boolean = js.native

  def to(properties: js.Object,
         duration: UndefOr[Int] = undefined,
         ease: UndefOr[String | js.Function] = undefined,
         autoStart: UndefOr[Boolean] = undefined,
         delay: UndefOr[Int] = undefined,
         repeat: UndefOr[Int] = undefined,
         yoyo: UndefOr[Boolean] = undefined): Tween = js.native
  def from(properties: js.Object,
           duration: UndefOr[Int] = undefined,
           ease: UndefOr[String | js.Function] = undefined,
           autoStart: UndefOr[Boolean] = undefined,
           delay: UndefOr[Int] = undefined,
           repeat: UndefOr[Int] = undefined,
           yoyo: UndefOr[Boolean] = undefined): Tween = js.native
  def start(index: UndefOr[Int] = undefined): Tween = js.native
  def stop(complete: UndefOr[Boolean] = undefined): Tween = js.native
  def updateTweenData(property: String,
                      value: Double | js.Function,
                      index: UndefOr[Int] = undefined): Tween = js.native
  def delay(duration: Int, index: UndefOr[Int] = undefined): Tween = js.native
  def repeat(total: Int,
             repeatDelay: UndefOr[Int] = undefined,
             index: UndefOr[Int] = undefined): Tween = js.native
  def repeatDelay(duration: Int, index: UndefOr[Int] = undefined): Tween =
    js.native
  def yoyo(enable: Boolean,
           yoyoDelay: UndefOr[Int] = undefined,
           index: UndefOr[Int] = undefined): Tween = js.native
  def yoyoDelay(duration: Int, index: UndefOr[Int] = undefined): Tween =
    js.native
  def easing(ease: String | js.Function,
             index: UndefOr[Int] = undefined): Tween = js.native
  def interpolation(interpolation: js.Function, context: UndefOr[js.Object] = undefined, index: UndefOr[Int] = undefined): Tween = js.native

  def repeatAll(total: UndefOr[Int] = undefined): Tween = js.native

  def chain(tweens: Tween*): Tween = js.native

  def loop(value: Boolean): Tween = js.native

  def onUpdateCallback(callback: js.Function, callbackContext: js.Object): Tween = js.native

  def pause(): Unit = js.native

  def resume(): Unit = js.native

  def generateData(frameRate: UndefOr[Int] = undefined, data: UndefOr[js.Array[js.Any]] = undefined): js.Array[js.Any] = js.native

  def totalDuration: Int = js.native
}
