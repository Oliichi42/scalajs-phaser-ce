package com.gitlab.oliichi42.phaserce.binding

import scala.scalajs.js
import scala.scalajs.js.annotation.{JSGlobal, JSName}

@JSGlobal("Phaser")
@js.native
object Phaser extends js.Object {

  /*
  Phaser Constants
   */
  val VERSION: String = js.native
  // TODO Games: []

  /*
  Renderer Constants
   */
  val AUTO: Int = js.native
  val CANVAS: Int = js.native
  val WEBGL: Int = js.native
  val HEADLESS: Int = js.native
  val WEBGL_MULTI: Int = js.native

  /*
  Direction Constants
   */
  val NONE: Int = js.native
  val LEFT: Int = js.native
  val RIGHT: Int = js.native
  val UP: Int = js.native
  val DOWN: Int = js.native

  /*
  Game Object Type Constants
   */
  val SPRITE: Int = js.native
  val BUTTON: Int = js.native
  val IMAGE: Int = js.native
  val GRPAHICS: Int = js.native
  val TEXT: Int = js.native
  val TILESPRITE: Int = js.native
  val BITMAPTEXT: Int = js.native
  val GROUP: Int = js.native
  val RENDERTEXTURE: Int = js.native
  val TILEMAP: Int = js.native
  val TILEMAPLAYER: Int = js.native
  val EMITTER: Int = js.native
  val POLYGON: Int = js.native
  val BITMAPDATA: Int = js.native
  val CANVAS_FILTER: Int = js.native
  val WEBGL_FILTER: Int = js.native
  val ELLIPSE: Int = js.native
  val SPRITEBATCH: Int = js.native
  val RETROFONT: Int = js.native
  val POINTER: Int = js.native
  val ROPE: Int = js.native
  val CIRCLE: Int = js.native
  val RECTANGLE: Int = js.native
  val MATRIX: Int = js.native
  val POINT: Int = js.native
  val ROUNDEDRECTANGLE: Int = js.native
  val CREATURE: Int = js.native
  val VIDEO: Int = js.native
  val PENDING_ATLAS: Int = js.native

  /*
  Orientation Constants
   */
  val HORIZONTAL: Int = js.native
  val VERTICAL: Int = js.native
  val LANDSCAPE: Int = js.native
  val PORTRAIT: Int = js.native

  /*
  Angle Constants
   */
  val ANGLE_UP: Int = js.native
  val ANGLE_DOWN: Int = js.native
  val ANGLE_LEFT: Int = js.native
  val ANGLE_RIGHT: Int = js.native
  val ANGLE_NORTH_EAST: Int = js.native
  val ANGLE_NORTH_WEST: Int = js.native
  val ANGLE_SOUTH_EAST: Int = js.native
  val ANGLE_SOUTH_WEST: Int = js.native

  /*
  Alignment/Positional Constants
   */
  val TOP_LEFT: Int = js.native
  @JSName("TOP_CENTER")
  val TOP_CENTRE: Int = js.native
  val TOP_RIGHT: Int = js.native
  val LEFT_TOP: Int = js.native
  @JSName("LEFT_CENTER")
  val LEFT_CENTRE: Int = js.native
  @JSName("CENTER")
  val CENTRE: Int = js.native
  val RIGHT_TOP: Int = js.native
  @JSName("RIGHT_CENTER")
  val RIGHT_CENTRE: Int = js.native
  val RIGHT_BOTTOM: Int = js.native
  val BOTTOM_LEFT: Int = js.native
  val BOTTOM_CENTRE: Int = js.native
  val BOTTOM_RIGHT: Int = js.native


  @js.native
  object blendModes extends js.Object {
    val NORMAL: Int = js.native
    val ADD: Int = js.native
    val MULTIPLY: Int = js.native
    val SCREEN: Int = js.native
    val OVERLAY: Int = js.native
    val DARKEN: Int = js.native
    val LIGHTEN: Int = js.native
    @JSName("COLOR_DODGE")
    val COLOUR_DODGE: Int = js.native
    @JSName("COLOR_BURN")
    val COLOUR_BURN: Int = js.native
    val HARD_LIGHT: Int = js.native
    val SOFT_LIGHT: Int = js.native
    val DIFFERENCE: Int = js.native
    val EXCLUSION: Int = js.native
    val HUE: Int = js.native
    val SATURATION: Int = js.native
    @JSName("COLOR")
    val COLOUR: Int = js.native
    val LUMINOSITY: Int = js.native
  }

  @js.native
  object scaleModes extends js.Object {
    val DEFAULT: Int = js.native
    val LINEAR: Int = js.native
    val NEAREST: Int = js.native
  }

  // TODO PIXI

  // _UID not exposed here

}
