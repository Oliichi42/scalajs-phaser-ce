package com.gitlab.oliichi42.phaserce.binding.geom

import scala.scalajs.js
import scala.scalajs.js.{UndefOr, undefined}
import scala.scalajs.js.annotation.{JSGlobal, JSName}

@js.native
@JSGlobal("Phaser.Point")
class Point() extends js.Object {
  @js.native
  def this(x: Double, y: Double) = this()

  var x, y: Double = js.native

  @JSName("type")
  val kind: Int = js.native


  def copyFrom(source: js.Object): Point = js.native
  def invert(): Point = js.native
  def setTo(x: Double, y: Double): Point = js.native
  def set(x: Double, y: Double): Point = js.native
  def setToPolar(azimuth: Double, radius: UndefOr[Double] = undefined, asDegrees: UndefOr[Boolean] = undefined): Point = js.native

  @JSName("add")
  def +(x: Double, y: Double): Point = js.native

  @JSName("subtract")
  def -(x: Double, y: Double): Point = js.native

  @JSName("multiply")
  def *(x: Double, y: Double): Point = js.native

  @JSName("divide")
  def /(x: Double, y: Double): Point = js.native

  def clampX(min: Double, max: Double): Point = js.native

  def clampY(min: Double, max: Double): Point = js.native

  def clamp(min: Double, max: Double): Point = js.native

  override def clone(): Point = js.native

  def clone(output: Point): Point = js.native

  def copyTo(dest: Point): Point = js.native

  def distance(dest: Point, round: UndefOr[Boolean] = undefined): Double = js.native

  def equals(o: Point): Boolean = js.native

  def angle(other: Point, asDegrees: UndefOr[Boolean] = undefined): Double = js.native

  def rotate(x: Double, y: Double, angle: Double, asDegrees: UndefOr[Boolean] = undefined, distance: UndefOr[Double] = undefined): Point = js.native

  @JSName("getMagnitude")
  def magnitude: Double = js.native

  @JSName("getMagnitudeSq")
  def magnitudeSq: Double = js.native

  def setMagnitude(newMagnitude: Double): Point = js.native

  @JSName("normalize")
  def normalise(): Point = js.native

  def limit(max: Double): Point = js.native

  def isZero: Boolean = js.native

  def dot(other: Point): Double = js.native

  def cross(other: Point): Double = js.native

  def perp(): Point = js.native

  def rperp(): Point = js.native

  def normalRightHand(): Point = js.native

  def floor(): Point = js.native

  def ceil(): Point = js.native

  override def toString: String = js.native


}

@js.native
@JSGlobal("Point.Phaser")
object Point extends js.Object {
  def add(a: Point, b: Point, out: UndefOr[Point] = undefined): Point = js.native
  def subtract(a: Point, b: Point, out: UndefOr[Point] = undefined): Point = js.native
  def multiply(a: Point, b: Point, out: UndefOr[Point] = undefined): Point = js.native
  def divide(a: Point, b: Point, out: UndefOr[Point] = undefined): Point = js.native

  def equals(a: Point, b: Point): Boolean = js.native
  def angle(a: Point, b: Point): Double = js.native
  def negative(a: Point, out: UndefOr[Point] = undefined): Point = js.native
  def multiplyAdd(a: Point, b: Point, s: Double, out: UndefOr[Point] = undefined): Point = js.native

  def interpolate(a: Point, b: Point, f: Double, out: UndefOr[Point] = undefined): Point = js.native
  def perp(a: Point, out: UndefOr[Point] = undefined): Point = js.native
  def rperp(a: Point, out: UndefOr[Point] = undefined): Point = js.native

  def distance(a: Point, b: Point, round: UndefOr[Boolean] = undefined): Double = js.native

  def project(a: Point, b: Point, out: UndefOr[Point] = undefined): Point = js.native
  def projectUnit(a: Point, b: Point, out: UndefOr[Point] = undefined): Point = js.native

  def normalRightHand(a: Point, out: UndefOr[Point] = undefined): Point = js.native
  @JSName("normalize")
  def normalise(a: Point, out: UndefOr[Point] = undefined): Point = js.native

  def rotate(a: Point, x: Double, y: Double, angle: Double, asDegrees: UndefOr[Boolean] = undefined, distance: UndefOr[Double] = undefined): Point = js.native

  def centroid(points: js.Array[Point], out: UndefOr[Point] = undefined): Point = js.native

  def parse(obj: js.Object, xProp: UndefOr[String] = undefined, yProp: UndefOr[String] = undefined): Point = js.native

  // isPoint

  def set(point: Point, x: Double, y: Double): Point = js.native

}