package com.gitlab.oliichi42.phaserce.binding.component

import scala.scalajs.js

@js.native
trait Angle extends js.Object {
  /*
  Angle in degrees.
   */
  var angle: Double = js.native
}
