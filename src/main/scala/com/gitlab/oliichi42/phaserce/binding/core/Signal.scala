package com.gitlab.oliichi42.phaserce.binding.core

import scala.scalajs.js
import scala.scalajs.js.annotation.JSGlobal
import scala.scalajs.js.{UndefOr, undefined}

@js.native
@JSGlobal("Phaser.Signal")
class Signal[F <: js.Function] extends js.Object {
  var active: Boolean = js.native

  def has(listener: F, context: UndefOr[js.Object] = undefined): Boolean = js.native
  def add(listener: F, listenerContext: UndefOr[js.Object] = undefined, priority: UndefOr[Int] = undefined): SignalBinding = js.native
  // we can't provide for this atm with type-safety def add(listener: F, listenerContext: UndefOr[js.Object], priority: UndefOr[Int], additionalArgs: js.Any*): SignalBinding = js.native
  def addOnce(listener: F, listenerContext: UndefOr[js.Object] = undefined, priority: UndefOr[Int] = undefined): SignalBinding = js.native
  // def addOnce(listener: F, listenerContext: UndefOr[js.Object], priority: UndefOr[Int], additionalArgs: js.Any*): SignalBinding = js.native

  def remove(listener: F, context: UndefOr[js.Object] = undefined): js.Function = js.native

  def removeAll(context: UndefOr[js.Object] = undefined): Unit = js.native

  def getNumListeners: Int = js.native

  def halt(): Unit = js.native

  def dispatch(): Unit = js.native

  def forget(): Unit = js.native

  def dispose(): Unit = js.native

  override def toString: String = js.native
}

@js.native
private trait SignalPrivate extends js.Object {

}

trait SignalHandler {

}
