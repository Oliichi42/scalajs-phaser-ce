package com.gitlab.oliichi42.phaserce.binding.utils

import scala.scalajs.js
import scala.scalajs.js.annotation.JSGlobal

@JSGlobal("Phaser.Debug")
@js.native
class Debug extends js.Object {
  // TODO add bindings
}
