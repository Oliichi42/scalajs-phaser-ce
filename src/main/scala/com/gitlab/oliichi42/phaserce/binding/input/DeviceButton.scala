package com.gitlab.oliichi42.phaserce.binding.input

import com.gitlab.oliichi42.phaserce.binding.core.{Game, Signal}

import scala.scalajs.js
import scala.scalajs.js.{UndefOr, undefined}

@js.native
trait DeviceButton extends js.Object {
  var buttonCode: Int = js.native
  var game: Game = js.native
  var isDown: Boolean = js.native
  var isUp: Boolean = js.native
  var onDown: Signal[_] = js.native
  var onFloat: Signal[_] = js.native
  var onUp: Signal[_] = js.native
  var pad: Gamepad = js.native
  var repeats: Int = js.native
  var timeDown: Double = js.native
  var timeUp: Double = js.native
  var value: Int = js.native
  def destroy(): Unit = js.native
  def justPressed(duration: UndefOr[Double] = undefined): Boolean = js.native
  def justReleased(duration: UndefOr[Double] = undefined): Boolean = js.native
  def reset(): Unit = js.native
}
