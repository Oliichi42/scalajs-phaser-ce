package com.gitlab.oliichi42.phaserce.binding.pixi

import com.gitlab.oliichi42.phaserce.binding.core.Filter
import com.gitlab.oliichi42.phaserce.binding.geom.{Matrix, Point, Rectangle}

import scala.scalajs.js

@js.native
trait Sprite extends js.Object {
  var anchor: Point = js.native
  // var texture: Texture
  /**
    * 0RGB Colour
    */
  var tint: Int = js.native
  // var tintedTexture: Canvas
  var blendMode: Int = js.native
  var shader: Filter = js.native
  var exists: Boolean = js.native

  var width, height: Double = js.native

  // setTexture
  // onTextureUpdate
  def getBounds(transformationMatrix: Matrix): Rectangle = js.native

  def getLocalBounds(): Rectangle = js.native
}
