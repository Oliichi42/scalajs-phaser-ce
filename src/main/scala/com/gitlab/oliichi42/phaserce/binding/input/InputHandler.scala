package com.gitlab.oliichi42.phaserce.binding.input

import com.gitlab.oliichi42.phaserce.binding.core.Game
import com.gitlab.oliichi42.phaserce.binding.gameobjects.Sprite
import com.gitlab.oliichi42.phaserce.binding.geom.{Point, Rectangle}

import scala.scalajs.js
import scala.scalajs.js.{UndefOr, undefined}
import scala.scalajs.js.annotation.{JSGlobal, JSName}

@js.native
@JSGlobal("Phaser.InputHandler")
class InputHandler extends js.Object {
  var sprite: Sprite = js.native
  var game: Game = js.native
  var enabled, useHandCursor: Boolean = js.native
  var priorityID: Int = js.native

  var isDragged, allowHorizontalDrag, allowVerticalDrag, bringToTop: Boolean =
    js.native

  var snapOffset: Point = js.native

  var snapOnDrag, snapOnRelease: Boolean = js.native

  var snapX, snapY, snapOffsetX, snapOffsetY: Double = js.native

  var pixelPerfectOver, pixelPerfectClick: Boolean = js.native

  var pixelPerfectAlpha: Int = js.native

  var draggable: Boolean = js.native

  var boundsRect: Rectangle = js.native

  var boundsSprite: Sprite = js.native

  // EXPERIMENTAL var scaleLayer: Boolean = js.native

  var dragOffset: Point = js.native

  @JSName("dragFromCenter")
  var dragFromCentre: Boolean = js.native

  var dragStopBlocksInputUp: Boolean = js.native

  var dragStartPoint: Point = js.native

  var dragDistanceThreshold, dragTimeThreshold: Int = js.native

  var downPoint, snapPoint: Point = js.native

  def start(priority: UndefOr[Int] = undefined,
            useHandCursor: UndefOr[Boolean] = undefined): Sprite = js.native

  def reset(): Unit = js.native

  def stop(): Unit = js.native

  def destroy(): Unit = js.native

  def isPixelPerfect: Boolean = js.native

  def pointerX(pointerId: UndefOr[Int] = undefined): Double = js.native
  def pointerY(pointerId: UndefOr[Int] = undefined): Double = js.native

  def pointerDown(pointerId: UndefOr[Int] = undefined): Boolean = js.native
  def pointerUp(pointerId: UndefOr[Int] = undefined): Boolean = js.native
  def pointerTimeDown(pointerId: UndefOr[Int] = undefined): Double = js.native
  def pointerTimeUp(pointerId: UndefOr[Int] = undefined): Double = js.native

  def pointerOver(pointerId: UndefOr[Int] = undefined): Boolean = js.native
  def pointerOut(pointerId: UndefOr[Int] = undefined): Boolean = js.native

  def pointerTimeOver(pointerId: UndefOr[Int] = undefined): Double = js.native
  def pointerTimeOut(pointerId: UndefOr[Int] = undefined): Double = js.native

  def pointerDragged(pointerId: UndefOr[Int] = undefined): Boolean = js.native
  def checkPointerDown(pointer: Pointer,
                       fastTest: UndefOr[Boolean] = undefined): Boolean =
    js.native
  def checkPointerOver(pointer: Pointer,
                       fastTest: UndefOr[Boolean] = undefined): Boolean =
    js.native

  def checkPixel(x: Double, y: Double): Boolean = js.native
  // also (null, null, pointer) available

  def justOver(pointerId: UndefOr[Int] = undefined, delay: Double): Boolean =
    js.native
  def justOut(pointerId: UndefOr[Int] = undefined, delay: Double): Boolean =
    js.native
  def justPressed(pointerId: UndefOr[Int] = undefined, delay: Double): Boolean =
    js.native
  def justReleased(pointerId: UndefOr[Int] = undefined,
                   delay: Double): Boolean = js.native

  def overDuration(pointerId: UndefOr[Int] = undefined): Boolean = js.native
  def downDuration(pointerId: UndefOr[Int] = undefined): Boolean = js.native

  def enableDrag(lockCentre: UndefOr[Boolean] = undefined,
                 bringToTop: UndefOr[Boolean] = undefined,
                 pixelPerfect: UndefOr[Boolean] = undefined,
                 alphaThreshold: UndefOr[Int] = undefined,
                 boundsRect: UndefOr[Rectangle] = undefined,
                 boundsSprite: UndefOr[Sprite] = undefined): Unit = js.native

  def disableDrag(): Unit = js.native

  // def startDrag() stopDrag()

  def globalToLocal(globalCoord: Point): Point = js.native

  def setDragLock(allowHorizontal: UndefOr[Boolean] = undefined, allowVertical: UndefOr[Boolean] = undefined): Unit = js.native

  def enableSnap(snapX: Double, snapY: Double, onDrag: UndefOr[Boolean] = undefined, onRelease: UndefOr[Boolean] = undefined, snapOffsetX: UndefOr[Double] = undefined, snapOffsetY: UndefOr[Double] = undefined): Unit = js.native

  def disableSnap(): Unit = js.native

  def checkBoundsRect(): Unit = js.native

  def checkBoundsSprite(): Unit = js.native

}
