package com.gitlab.oliichi42.phaserce.binding.gameobjects

import com.gitlab.oliichi42.phaserce.binding.component.{Angle, Events, InputEnabled}
import com.gitlab.oliichi42.phaserce.binding.pixi

import scala.scalajs.js
import scala.scalajs.js.annotation.JSGlobal

@JSGlobal("Phaser.Sprite")
@js.native
class Sprite extends js.Object with Angle with pixi.Sprite with InputEnabled {
  // TODO

  def events: Events = js.native

}
