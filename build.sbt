name := "scalajs-phaser-ce"

organization := "com.gitlab.oliichi42"

version := "2.8.3-SNAPSHOT"

scalaVersion := "2.12.2"


val phaserCEVersion = "2.8.3"


libraryDependencies ++= Seq(

  // WebJars is used to package the phaser-ce NPM package
  "org.webjars.npm" % "phaser-ce" % phaserCEVersion

)

jsDependencies += "org.webjars.npm" % "phaser-ce" % phaserCEVersion / "build/phaser.js"



scalacOptions ++= Seq(
  "-deprecation",
  "-unchecked",
  "-feature",
  "-encoding",
  "utf8"
)

enablePlugins(ScalaJSPlugin)
// when Scala.js 1.x comes around:
// enablePlugins(JSDependenciesPlugin)

